<?xml version="1.0" encoding="utf-8" standalone="no"?>
<WMS_Capabilities version="1.3.0" xmlns="http://www.opengis.net/wms" xmlns:sld="http://www.opengis.net/sld" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ms="http://mapserver.gis.umn.edu/mapserver" xsi:schemaLocation="http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd  http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/sld_capabilities.xsd  http://mapserver.gis.umn.edu/mapserver">
  <Service>
    <Name>WMS</Name>
    <Title>orto_foraar</Title>
    <Abstract>SDFE Kortforsyningen OGC:WMS - Ortofoto forår</Abstract>
    <KeywordList>
      <Keyword>ortofoto</Keyword>
      <Keyword>forår</Keyword>
      <Keyword>foraar</Keyword>
      <Keyword>geodanmark</Keyword>
    </KeywordList>
    <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar/1.0.0/Wms?username=xxxx&amp;password=xxxx&amp;" />
    <ContactInformation></ContactInformation>
    <MaxWidth>10000</MaxWidth>
    <MaxHeight>10000</MaxHeight>
  </Service>
  <Capability>
    <Request>
      <GetCapabilities>
        <Format>text/xml</Format>
        <DCPType>
          <HTTP>
            <Get>
              <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar/1.0.0/Wms?username=xxxx&amp;password=xxxx&amp;" />
            </Get>
          </HTTP>
        </DCPType>
      </GetCapabilities>
      <GetMap>
        <Format>image/jpeg</Format>
        <Format>image/png</Format>
        <DCPType>
          <HTTP>
            <Get>
              <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar/1.0.0/Wms?username=xxxx&amp;password=xxxx&amp;" />
            </Get>
          </HTTP>
        </DCPType>
      </GetMap>
    </Request>
    <Exception>
      <Format>XML</Format>
    </Exception>
    <Layer>
      <Name>GeoDanmark ortofoto</Name>
      <Title>orto_foraar</Title>
      <Abstract>SDFE Kortforsyningen OGC:WMS - Ortofoto forår</Abstract>
      <KeywordList>
        <Keyword>ortofoto</Keyword>
        <Keyword>forår</Keyword>
        <Keyword>foraar</Keyword>
        <Keyword>geodanmark</Keyword>
      </KeywordList>
      <CRS>EPSG:25832</CRS>
      <CRS>EPSG:25833</CRS>
      <CRS>EPSG:32632</CRS>
      <CRS>EPSG:32633</CRS>
      <CRS>EPSG:4326</CRS>
      <CRS>EPSG:4258</CRS>
      <CRS>EPSG:4093</CRS>
      <CRS>EPSG:4094</CRS>
      <CRS>EPSG:4095</CRS>
      <CRS>EPSG:4096</CRS>
      <CRS>EPSG:3395</CRS>
      <CRS>EPSG:3857</CRS>
      <CRS>EPSG:2196</CRS>
      <CRS>EPSG:2197</CRS>
      <CRS>EPSG:2198</CRS>
      <EX_GeographicBoundingBox>
        <westBoundLongitude>2.47842</westBoundLongitude>
        <eastBoundLongitude>17.5578</eastBoundLongitude>
        <southBoundLatitude>53.015</southBoundLatitude>
        <northBoundLatitude>58.6403</northBoundLatitude>
      </EX_GeographicBoundingBox>
      <BoundingBox CRS="EPSG:25832" minx="120000" miny="5.9e+06" maxx="1e+06" maxy="6.5e+06" />
      <BoundingBox CRS="EPSG:25833" minx="-280235" miny="5.87493e+06" maxx="649671" maxy="6.54973e+06" />
      <BoundingBox CRS="EPSG:32632" minx="120000" miny="5.9e+06" maxx="1e+06" maxy="6.5e+06" />
      <BoundingBox CRS="EPSG:32633" minx="-280235" miny="5.87493e+06" maxx="649671" maxy="6.54973e+06" />
      <BoundingBox CRS="EPSG:4326" minx="53.015" miny="2.47842" maxx="58.6403" maxy="17.5578" />
      <BoundingBox CRS="EPSG:4258" minx="53.015" miny="2.47842" maxx="58.6403" maxy="17.5578" />
      <BoundingBox CRS="EPSG:4093" minx="-180144" miny="902243" maxx="700190" maxy="1.50247e+06" />
      <BoundingBox CRS="EPSG:4094" minx="-46994" miny="895713" maxx="841906" maxy="1.50858e+06" />
      <BoundingBox CRS="EPSG:4095" minx="36116.8" miny="886552" maxx="939765" maxy="1.52136e+06" />
      <BoundingBox CRS="EPSG:4096" minx="19452.9" miny="877286" maxx="949731" maxy="1.55235e+06" />
      <BoundingBox CRS="EPSG:3395" minx="275897" miny="6.95162e+06" maxx="1.95452e+06" maxy="8.06652e+06" />
      <BoundingBox CRS="EPSG:3857" minx="275897" miny="6.98577e+06" maxx="1.95452e+06" maxy="8.10304e+06" />
      <BoundingBox CRS="EPSG:2196" minx="-213561" miny="5.89868e+06" maxx="671042" maxy="6.50522e+06" />
      <BoundingBox CRS="EPSG:2197" minx="-80552.6" miny="5.8853e+06" maxx="825151" maxy="6.52321e+06" />
      <BoundingBox CRS="EPSG:2198" minx="119453" miny="5.87729e+06" maxx="1.04973e+06" maxy="6.55235e+06" />
      <Layer>
        <Name>orto_foraar</Name>
        <Title>orto_foraar</Title>
        <Abstract>orto_foraar</Abstract>
        <Style>
          <Name>default</Name>
          <Title>default</Title>
        </Style>
        <Layer queryable="0" opaque="0" cascaded="0">
          <Name>orto_foraar_12_5</Name>
          <Title>Ortofoto forår 12,5cm</Title>
          <CRS>epsg:25832</CRS>
          <EX_GeographicBoundingBox>
            <westBoundLongitude>7.99125</westBoundLongitude>
            <eastBoundLongitude>15.6162</eastBoundLongitude>
            <southBoundLatitude>54.4258</southBoundLatitude>
            <northBoundLatitude>57.778</northBoundLatitude>
          </EX_GeographicBoundingBox>
          <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>1e+08</MaxScaleDenominator>
        </Layer>
        <Layer queryable="0" opaque="0" cascaded="0">
          <Name>orto_foraar_10</Name>
          <Title>Ortofoto forår 10cm</Title>
          <CRS>epsg:25832</CRS>
          <EX_GeographicBoundingBox>
            <westBoundLongitude>7.99125</westBoundLongitude>
            <eastBoundLongitude>15.6162</eastBoundLongitude>
            <southBoundLatitude>54.4258</southBoundLatitude>
            <northBoundLatitude>57.778</northBoundLatitude>
          </EX_GeographicBoundingBox>
          <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>1e+08</MaxScaleDenominator>
        </Layer>
      </Layer>
      <Layer>
        <Name>orto_foraar_cir</Name>
        <Title>orto_foraar_cir</Title>
        <Abstract>orto_foraar_cir</Abstract>
        <Style>
          <Name>default</Name>
          <Title>default</Title>
        </Style>
        <Layer queryable="0" opaque="0" cascaded="0">
          <Name>orto_foraar_12_5_cir</Name>
          <Title>Ortofoto forår 12,5cm, CIR</Title>
          <Abstract>Datagrundlaget til dette lag er det samme som orto_foraar_12_5, men i stedet for RGB, vises kanalerne NirRG</Abstract>
          <CRS>epsg:25832</CRS>
          <EX_GeographicBoundingBox>
            <westBoundLongitude>7.99125</westBoundLongitude>
            <eastBoundLongitude>15.6162</eastBoundLongitude>
            <southBoundLatitude>54.4258</southBoundLatitude>
            <northBoundLatitude>57.778</northBoundLatitude>
          </EX_GeographicBoundingBox>
          <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>1e+08</MaxScaleDenominator>
        </Layer>
        <Layer queryable="0" opaque="0" cascaded="0">
          <Name>orto_foraar_10_cir</Name>
          <Title>Ortofoto forår 10cm, CIR</Title>
          <Abstract>Datagrundlaget til dette lag er det samme som orto_foraar_10, men i stedet for RGB, vises kanalerne NirRG</Abstract>
          <CRS>epsg:25832</CRS>
          <EX_GeographicBoundingBox>
            <westBoundLongitude>7.99125</westBoundLongitude>
            <eastBoundLongitude>15.6162</eastBoundLongitude>
            <southBoundLatitude>54.4258</southBoundLatitude>
            <northBoundLatitude>57.778</northBoundLatitude>
          </EX_GeographicBoundingBox>
          <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>1e+08</MaxScaleDenominator>
        </Layer>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex</Name>
        <Title>Optagetidspunkt</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2021_12_5cm</Name>
        <Title>geodanmark_2021_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2021_12_5cm_cir</Name>
        <Title>geodanmark_2021_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2021_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2021_10cm</Name>
        <Title>geodanmark_2021_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2021_10cm_cir</Name>
        <Title>geodanmark_2021_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2021_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2021</Name>
        <Title>Optagetidspunkt 2021</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2020_12_5cm</Name>
        <Title>geodanmark_2020_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2020_12_5cm_cir</Name>
        <Title>geodanmark_2020_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2020_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2020_10cm</Name>
        <Title>geodanmark_2020_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2020_10cm_cir</Name>
        <Title>geodanmark_2020_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2020_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2020</Name>
        <Title>Optagetidspunkt 2020</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2019_12_5cm</Name>
        <Title>geodanmark_2019_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2019_12_5cm_cir</Name>
        <Title>geodanmark_2019_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2019_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2019_10cm</Name>
        <Title>geodanmark_2019_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2019_10cm_cir</Name>
        <Title>geodanmark_2019_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2019_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2019</Name>
        <Title>Optagetidspunkt 2019</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2018_12_5cm</Name>
        <Title>geodanmark_2018_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2018_12_5cm_cir</Name>
        <Title>geodanmark_2018_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2018_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2018_10cm</Name>
        <Title>geodanmark_2018_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2018_10cm_cir</Name>
        <Title>geodanmark_2018_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2018_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2018</Name>
        <Title>Optagetidspunkt 2018</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2017_12_5cm</Name>
        <Title>geodanmark_2017_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2017_12_5cm_cir</Name>
        <Title>geodanmark_2017_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2017_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2017_10cm</Name>
        <Title>geodanmark_2017_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2017_10cm_cir</Name>
        <Title>geodanmark_2017_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2017_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2017</Name>
        <Title>Optagetidspunkt 2017</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2016_12_5cm</Name>
        <Title>geodanmark_2016_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.991</westBoundLongitude>
          <eastBoundLongitude>15.651</eastBoundLongitude>
          <southBoundLatitude>53.1013</southBoundLatitude>
          <northBoundLatitude>57.787</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="5.9e+06" maxx="897000" maxy="6.405e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2016_12_5cm_cir</Name>
        <Title>geodanmark_2016_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2016_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.991</westBoundLongitude>
          <eastBoundLongitude>15.651</eastBoundLongitude>
          <southBoundLatitude>53.1013</southBoundLatitude>
          <northBoundLatitude>57.787</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="5.9e+06" maxx="897000" maxy="6.405e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>fotoindex_2016</Name>
        <Title>Optagetidspunkt 2016</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.99125</westBoundLongitude>
          <eastBoundLongitude>15.6162</eastBoundLongitude>
          <southBoundLatitude>54.4258</southBoundLatitude>
          <northBoundLatitude>57.778</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.048e+06" maxx="895000" maxy="6.404e+06" />
        <Style>
          <Name>Optagetidspunkt</Name>
          <Title>Optagetidspunkt</Title>
        </Style>
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>160000</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>hrks_2015_10cm</Name>
        <Title>hrks_2015_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>11.5827</westBoundLongitude>
          <eastBoundLongitude>12.8835</eastBoundLongitude>
          <southBoundLatitude>55.0154</southBoundLatitude>
          <northBoundLatitude>56.2133</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="665000" miny="6.103e+06" maxx="741000" maxy="6.233e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>hrks_2015_10cm_cir</Name>
        <Title>hrks_2015_10cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”hrks_2015_10cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>11.5827</westBoundLongitude>
          <eastBoundLongitude>12.8835</eastBoundLongitude>
          <southBoundLatitude>55.0154</southBoundLatitude>
          <northBoundLatitude>56.2133</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="665000" miny="6.103e+06" maxx="741000" maxy="6.233e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2015_12_5cm</Name>
        <Title>geodanmark_2015_12_5cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.991</westBoundLongitude>
          <eastBoundLongitude>15.651</eastBoundLongitude>
          <southBoundLatitude>53.1013</southBoundLatitude>
          <northBoundLatitude>57.787</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="5.9e+06" maxx="897000" maxy="6.405e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geodanmark_2015_12_5cm_cir</Name>
        <Title>geodanmark_2015_12_5cm_cir</Title>
        <Abstract>Datagrundlaget til dette lag er det samme som ”geodanmark_2015_12_5cm”, men i stedet for RGB, vises kanalerne NirRG</Abstract>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>7.991</westBoundLongitude>
          <eastBoundLongitude>15.651</eastBoundLongitude>
          <southBoundLatitude>53.1013</southBoundLatitude>
          <northBoundLatitude>57.787</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="5.9e+06" maxx="897000" maxy="6.405e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geomidt_2014_10cm</Name>
        <Title>geomidt_2014_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>8.67207</westBoundLongitude>
          <eastBoundLongitude>11.7365</eastBoundLongitude>
          <southBoundLatitude>55.6471</southBoundLatitude>
          <northBoundLatitude>56.8528</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="480000" miny="6.17e+06" maxx="667000" maxy="6.301e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>hrks_2014_10cm</Name>
        <Title>hrks_2014_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>11.5827</westBoundLongitude>
          <eastBoundLongitude>15.3831</eastBoundLongitude>
          <southBoundLatitude>54.9156</southBoundLatitude>
          <northBoundLatitude>56.2133</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="665000" miny="6.103e+06" maxx="897000" maxy="6.233e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geosjaelland_2014_10cm</Name>
        <Title>geosjaelland_2014_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>10.7946</westBoundLongitude>
          <eastBoundLongitude>12.6549</eastBoundLongitude>
          <southBoundLatitude>54.5371</southBoundLatitude>
          <northBoundLatitude>56.0212</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="616000" miny="6.049e+06" maxx="728000" maxy="6.21e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geofyn_2013_10cm</Name>
        <Title>geofyn_2013_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>9.61992</westBoundLongitude>
          <eastBoundLongitude>11.0645</eastBoundLongitude>
          <southBoundLatitude>54.6346</southBoundLatitude>
          <northBoundLatitude>55.6382</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="540000" miny="6.056e+06" maxx="630000" maxy="6.166e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geomidt_2013_10cm</Name>
        <Title>geomidt_2013_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>8.01979</westBoundLongitude>
          <eastBoundLongitude>10.4865</eastBoundLongitude>
          <southBoundLatitude>55.6673</southBoundLatitude>
          <northBoundLatitude>56.718</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="440000" miny="6.17e+06" maxx="591000" maxy="6.286e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>hrks_2013_10cm</Name>
        <Title>hrks_2013_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>11.5827</westBoundLongitude>
          <eastBoundLongitude>15.3831</eastBoundLongitude>
          <southBoundLatitude>54.9156</southBoundLatitude>
          <northBoundLatitude>56.2133</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="665000" miny="6.103e+06" maxx="897000" maxy="6.233e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geosjaelland_2013_10cm</Name>
        <Title>geosjaelland_2013_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>10.7946</westBoundLongitude>
          <eastBoundLongitude>12.6549</eastBoundLongitude>
          <southBoundLatitude>54.5371</southBoundLatitude>
          <northBoundLatitude>56.0212</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="616000" miny="6.049e+06" maxx="728000" maxy="6.21e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>hrks_2012_10cm</Name>
        <Title>hrks_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>11.6173</westBoundLongitude>
          <eastBoundLongitude>12.8835</eastBoundLongitude>
          <southBoundLatitude>55.5444</southBoundLatitude>
          <northBoundLatitude>56.2133</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="665000" miny="6.162e+06" maxx="741000" maxy="6.233e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geosjaelland_2012_10cm</Name>
        <Title>geosjaelland_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>10.7958</westBoundLongitude>
          <eastBoundLongitude>12.6549</eastBoundLongitude>
          <southBoundLatitude>54.564</southBoundLatitude>
          <northBoundLatitude>56.0212</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="616000" miny="6.052e+06" maxx="728000" maxy="6.21e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>viborg_skive_2012_10cm</Name>
        <Title>viborg_skive_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>8.67207</westBoundLongitude>
          <eastBoundLongitude>9.8034</eastBoundLongitude>
          <southBoundLatitude>56.2124</southBoundLatitude>
          <northBoundLatitude>56.8528</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="480000" miny="6.23e+06" maxx="549000" maxy="6.301e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geonord_2012_10cm</Name>
        <Title>geonord_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>8.1087</westBoundLongitude>
          <eastBoundLongitude>11.2526</eastBoundLongitude>
          <southBoundLatitude>56.4923</southBoundLatitude>
          <northBoundLatitude>57.787</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="447000" miny="6.263e+06" maxx="634000" maxy="6.405e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>sydkort_2012_10cm</Name>
        <Title>sydkort_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>8.43962</westBoundLongitude>
          <eastBoundLongitude>10.1047</eastBoundLongitude>
          <southBoundLatitude>54.7904</southBoundLatitude>
          <northBoundLatitude>55.9454</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="465000" miny="6.072e+06" maxx="569000" maxy="6.2e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
      <Layer queryable="0" opaque="0" cascaded="0">
        <Name>geofyn_2012_10cm</Name>
        <Title>geofyn_2012_10cm</Title>
        <CRS>epsg:25832</CRS>
        <EX_GeographicBoundingBox>
          <westBoundLongitude>9.62088</westBoundLongitude>
          <eastBoundLongitude>10.9851</eastBoundLongitude>
          <southBoundLatitude>54.6988</southBoundLatitude>
          <northBoundLatitude>55.6382</northBoundLatitude>
        </EX_GeographicBoundingBox>
        <BoundingBox CRS="epsg:25832" minx="540000" miny="6.063e+06" maxx="625000" maxy="6.166e+06" />
        <MinScaleDenominator>1</MinScaleDenominator>
        <MaxScaleDenominator>1e+08</MaxScaleDenominator>
      </Layer>
    </Layer>
  </Capability>
</WMS_Capabilities>