import json

def List_of_muni():
    with open('municipalities.geojson') as f:
        data = json.load(f)

    byerlist = []
    for feature in data['features']:

        byer = feature['properties']['label_dk']
        byerlist.append(byer)

    return sorted(set(byerlist))    # Christiansø


