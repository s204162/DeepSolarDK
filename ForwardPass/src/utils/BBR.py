# Extracting positive predictions for a specific municipality

import pandas as pd
from itertools import chain
from shapely.geometry import Polygon
from shapely.ops import unary_union
import json
from shapely.geometry import Point
import yaml
#from utilities.Get_poly_to_CSV import definePolygon
from src.utils.geojson_handler_utils import GeoJsonHandler

# Script for creating CSV-file with BBR's known coordinates with solarpanel in af municipality.

muni = "Herlev"

Municipality = GeoJsonHandler(input_dir='../../municipalities.geojson', input_munic=muni)

# Open data
with open('../../bbr_solar.csv', 'r') as f:
    d = pd.read_csv(f)

# Get coords with solarpanels within muni's polygon
new_shape = Municipality.definePolygon()
solarp_for_muni = []
for (a,b) in zip(d['lat'],d['lon']):
    LL = Point(b, a)
    if new_shape.intersects(LL):
        solarp_for_muni.append([b,a])

# Write into csv-file
import csv
data = solarp_for_muni
#print(data)
file = open(f'../../x{muni}_data/BBR_{muni}.csv', 'w+', newline ='')
with file:
    write = csv.writer(file)
    write.writerow(['lon','lat'])
    write.writerows(data)

print("There are", len(solarp_for_muni), "solar images in",  muni, '\n', "The data from BBR is saved in " + f'x{muni}_data/BBR_{muni}.csv')
