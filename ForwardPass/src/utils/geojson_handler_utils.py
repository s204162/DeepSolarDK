import json
from itertools import chain
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import pickle
import numpy as np
import matplotlib.pyplot as plt
from shapely.ops import unary_union


class GeoJsonHandler(object):

    def __init__(self, input_dir, input_munic):      # HER er ændret

        self.geojson_dir = input_dir

        with open(self.geojson_dir) as f:

            self.data = json.load(f)

        self.munic = input_munic                 # HER er ændret
        self.polygon = self.definePolygon()

        self.bounds = self.GetBounds()

    def definePolygon(self):

        polygons = []
        polygons_tuple = []
        # Load geojson for all munis and select specific muni.
        for feature in self.data['features']:

            # Select the state of NRW
            if feature['properties']['label_en'] == self.munic:      # HER er ændret
                # list containing all NRW coordinates
                nrw_coords = feature['geometry']['coordinates']

        # Unlist nrw_coords
                nrw_coords = list(chain(*nrw_coords))

                nrw_coords_tuples = [tuple(elem) for elem in nrw_coords]
                polygons_tuple.append(nrw_coords_tuples)


        # It is a closed polygon
        # TODO add assert statement to verify that it is a closed polygon

        # Ensure we get list of all polygons
        # We use Polygon() to get these
        [polygons.append(Polygon(eachpoly)) for eachpoly in polygons_tuple]

        #___________Make multipolygons____________
        multipolygon = unary_union(polygons)


        return multipolygon

    def GetBounds(self):
        coordi = []
        for feature in self.data['features']:

            byer = feature['properties']['label_dk']
            if byer == self.munic:
                # Ensure all data is obtained
                coordi += feature['geometry']['coordinates'][0]
        # Get East, North, West, South
        E = np.max(coordi, axis=0)[0]
        N = np.max(coordi, axis=0)[1]
        W = np.min(coordi, axis=0)[0]
        S = np.min(coordi, axis=0)[1]

        return [E,N,W,S]

    def returnTileCoords(self, path_to_pickle):

        # Tile_coords specifies almost 600,000 tiles.
        with open(path_to_pickle, "rb") as f:

            Tile_coords = pickle.load(f)

        return Tile_coords

    def plot_muni(self):
        polygons = self.polygon
        #E,N,W,S = self.GetBounds()
        #Box = Polygon([(E,N),(E,S),(W,S),(W,N)])
        #plt.plot(*Box.exterior.xy)
        plt.plot(*polygons.exterior.xy)
        plt.axis('square')

        # Set (current) axis to be equal before showing plot
        plt.gca().axis("equal")
        plt.axis('square')
        plt.savefig(f"../Plots/{self.munic}_PlotOfMuni.png")

        plt.show()


