import json
from itertools import chain
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import pickle
import numpy as np
import matplotlib.pyplot as plt
from shapely.ops import unary_union


class GeoJsonHandlerRegion(object):
    #init takes input of .geojson file, and which region we want to target.
    def __init__(self, input_dir, input_region):

        self.geojson_dir = input_dir

        with open(self.geojson_dir) as f:

            self.data = json.load(f)

        self.region = input_region
        self.polygon = self.definePolygon()

        self.bounds = self.GetBounds()

    def definePolygon(self):

        polygons = []
        polygons_tuple = []
        # Load geojson for all munis and select specific muni, based on region
        for feature in self.data['features']:

            # Select the state of NRW
            if feature['properties']['iso_3166_2'] == self.region:
                # list containing all NRW coordinates
                region_coords = feature['geometry']['coordinates']

                region_coords = list(chain(*region_coords))

                region_coords_tuples = [tuple(elem) for elem in region_coords]
                polygons_tuple.append(region_coords_tuples)


        # It is a closed polygon
        # TODO add assert statement to verify that it is a closed polygon

        # Ensure we get list of all polygons
        # We use Polygon() to get these
        [polygons.append(Polygon(eachpoly)) for eachpoly in polygons_tuple]

        #___________Make multipolygons____________
        multipolygon = unary_union(polygons)


        return multipolygon

    def GetBounds(self):
        coordi = []
        for feature in self.data['features']:

            byer = feature['properties']['iso_3166_2']
            if byer == self.region:
                # Ensure all data is obtained
                coordi += feature['geometry']['coordinates'][0]
        # Get East, North, West, South
        E = np.max(coordi, axis=0)[0]
        N = np.max(coordi, axis=0)[1]
        W = np.min(coordi, axis=0)[0]
        S = np.min(coordi, axis=0)[1]

        return [E,N,W,S]

    def returnTileCoords(self, path_to_pickle):

        # Tile_coords specifies almost 600,000 tiles.
        with open(path_to_pickle, "rb") as f:

            Tile_coords = pickle.load(f)

        return Tile_coords

    def plot_muni(self):
        polygons = self.polygon
        #E,N,W,S = self.GetBounds()
        #Box = Polygon([(E,N),(E,S),(W,S),(W,N)])
        #plt.plot(*Box.exterior.xy)

        fig, axs = plt.subplots()
        axs.set_aspect('equal', 'datalim')

        for geom in polygons.geoms:
            xs, ys = geom.exterior.xy
            axs.fill(xs, ys, alpha=0.5, fc='r', ec='none')


        # Set (current) axis to be equal before showing plot
        plt.title('Nordjylland')
        plt.savefig(f"{self.region}_PlotOfMuni.png")
        plt.show()


region = GeoJsonHandlerRegion('C:/Users/juliu/OneDrive/DTU/Fagprojekt/DeepSolarDK/ForwardPass/municipalities.geojson', "DK-81")
#
region.plot_muni()
# print(region.GetBounds())

# "DK-81" Nordjylland
# "DK-82" Midtjylland
# "DK-83" Syddanmark
# "DK-84" Hovedstaden + Bornholm
# "DK-85" Sjælland

