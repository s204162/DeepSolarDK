from __future__ import unicode_literals
import yaml
from pathlib import Path
import os
from prompt_toolkit import prompt
import pandas as pd

from src.pipeline_components.tile_creator import TileCreator
from src.pipeline_components.tile_downloader import TileDownloader
from src.pipeline_components.tile_processor import TileProcessor
from src.pipeline_components.tile_updater import TileCoordsUpdater
from src.pipeline_components.Get_all_muni import List_of_muni
from src.utils.geojson_handler_utils import GeoJsonHandler
from src.utils.geojson_handler_utils_region import GeoJsonHandlerRegion
from shapely.geometry import Polygon
from shapely.ops import cascaded_union, unary_union

def main():

    # ------- Read configuration -------

    config_file = 'config.yml'


    use_region = True
    municipality = 'Gentofte'
    region = 'DK-84'

    if use_region:
        municipality = region


    with open(config_file, 'rb') as f:

        conf = yaml.load(f, Loader=yaml.FullLoader)
    run_tile_creator = False
    run_tile_downloader = False
    run_tile_processor = False
    run_tile_updater = False
    run_training = False

    tile_coords_path = f"data/coords/Tile_coords_{municipality}.pickle"
    geojson_path = conf.get('geojson_path', "municipalities.geojson")
    downloaded_path = conf.get('downloaded_path', 'logs/processing/DownloadedTiles.csv')
    processed_path = conf.get('processed_path','logs/processing/Processed.csv')


    # ------- GeoJsonHandler provides utility functions -------
    if use_region:
        municipality_handler = GeoJsonHandlerRegion(geojson_path, region)
    else:
        municipality_handler = GeoJsonHandler(geojson_path, municipality)

    # ------- TileCreator creates pickle file with all tiles in municipality and their respective minx, miny, maxx, maxy coordinates -------



    if run_tile_creator:
        print(f"Starting to create a pickle file with all bounding box coordinates for tiles within {municipality} ... This will take a while")
        tileCreator = TileCreator(outpath=tile_coords_path, polygon=municipality_handler.polygon,
                                  bounds=municipality_handler.bounds)
        tileCreator.defineTileCoords()


        print('Pickle file has been sucessfully created')
    # Tile_coords is a list of tuples. Each tuple specifies its respective tile by minx, miny, maxx, maxy.
    tile_coords = municipality_handler.returnTileCoords(path_to_pickle=Path(tile_coords_path))

    print(f'{len(tile_coords)} tiles have been identified.')

    # ------- TileDownloader downloads tiles from openNRW in a multi-threaded fashion -------

    if run_tile_downloader:

        print('Starting to download ' + str(len(tile_coords)) + '. This will take a while.')

        downloader = TileDownloader(configuration=conf, polygon=municipality_handler.polygon, tile_coords=tile_coords)

    if os.path.exists(Path(downloaded_path)):

        # Load DownloadedTiles.csv file
        downloadedTiles_df = pd.read_table(downloaded_path, header=None)

        print(f"{downloadedTiles_df[0].nunique()} unique tiles have been successfully downloaded.")

    if run_tile_processor:

        tileProcessor = TileProcessor(configuration=conf, polygon=municipality_handler.polygon)

        tileProcessor.run()

    if os.path.exists(processed_path):

        # Load DownloadedTiles.csv file
        processedTiles_df = pd.read_table(processed_path, header=None)

        print(f"{processedTiles_df[0].nunique()} unique tiles have been successfully processed.")

    if run_tile_updater:

        updater = TileCoordsUpdater(configuration=conf, tile_coords=tile_coords)

        updater.update()

if __name__ == '__main__':
    main()