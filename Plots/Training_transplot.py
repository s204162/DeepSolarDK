import torch
from torch import nn
#from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor
#from __future__ import print_function, division
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE" #Kan fjernes
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import torchvision
import torch.nn.functional as F
from torchvision import transforms, utils
from torchvision.datasets import ImageFolder
import PIL as img

class Trainer:
    """ This class takes a Tileprocessor object,training data. To train the model on the given training data,
    call Trainer.run_training(epochs) to display bar set bar = True"""

    def __init__(self,dataloader,TileProcessor,optimizer = torch.optim.Adam, weights = torch.tensor([0.5, 0.5])):
        self.data = dataloader #Pytorch dataloader
        self.TileProcessor = TileProcessor #Give the model.
        self.model = self.TileProcessor.model
        self.parameters = self.model.parameters
        self.optimizer = optimizer(self.parameters())
        self.loss = 0
        self.weights = weights
        self.loss_fn = nn.CrossEntropyLoss(weight=self.weights)  # Definer med class imbalance weights
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        #self.savepath = "models/"

    def train(self,lr):
    #Takes data as a pytorch dataloader, the model, loss and optimizer
     #self.model.train()
     for batch, (X,y) in enumerate(self.data):
         yp = torch.zeros(len(y), 2)
         for u in range(len(y)):
             if y[u] == 1:
                 yp[u, :] = torch.tensor([0, 1])
             if y[u] == 0:
                 yp[u, :] = torch.tensor([1, 0])
                 #Use softmax to get preds
         #out = self.model(X).logits.to(self.device)

         grid = torchvision.utils.make_grid(X, nrow=5)
         transforms.ToPILImage()(grid).show()


         pred = F.softmax(out, dim = 1)
         self.loss = self.loss_fn(pred, yp.to(float))

         #Back prop
         self.optimizer.zero_grad()
         self.loss.backward()
         self.optimizer.step()
         print(f'batch: {batch}')

    def run_training(self,epochs):
        #import tqdm
        for t in range(epochs):
            self.train(lr = 0.001)
            print(f'epoch {t}')

        print("Trainning Done!")
      #Save model params

    def save_params(self):
        path = os.path.join(self.savepath, 'HerlevWeights.pth.tar')
        torch.save(self.model.state_dict(), path)
        print(f'Saved PyTorch Model State to{self.savepath}')

def find_data(rootpath, batch_size, Shuffle = False):

    trans = transforms.Compose([
        #transforms.Resize(299),
        transforms.ToTensor(),
        #transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.RandomVerticalFlip(p=0.5),
        #transforms.RandomRotation([0, 90, 180, 270]),
            # Choosing saturation value uniformly in the range 0-1:
        transforms.ColorJitter(saturation=(0.8,1.2)) # brightness=0, contrast=0, saturation=(0,2), hue=0

    ])

    dataset = ImageFolder(root=rootpath,transform=trans)

    data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle = Shuffle)

    return data_loader

    #return X, y = next(iter(data_loader))

if __name__ == '__main__':
    from src.pipeline_components.tile_processor import TileProcessor
    from src.utils.geojson_handler_utils import GeoJsonHandler
    import yaml

    # ------- Read configuration -------

    config_file = '../config.yml'

    with open(config_file, 'rb') as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)

    run_tile_processor = False


    tile_coords_path = conf.get('tile_coords_path', 'data/coords/TileCoords.pickle')
    geojson_path = conf.get('geojson_path', "../municipalities.geojson")
                            #'utils/deutschlandGeoJSON/2_bundeslaender/1_sehr_hoch.geo.json')
    downloaded_path = conf.get('downloaded_path', 'logs/processing/DownloadedTiles.csv')
    processed_path = conf.get('processed_path', 'logs/processing/Processed.csv')

    municipality = 'Herlev'  # HER er ændret

    # ------- GeoJsonHandler provides utility functions -------

    municipality_handler = GeoJsonHandler(geojson_path, municipality)  # HER er ændret


    #Train model on a dataset from "root" location.
    batch_size = 8
    epochs = 1
    TileProcessor = TileProcessor(configuration = conf, polygon=municipality_handler.polygon)

    root = "data/datatest/" #Kun et testset for at se om programmet virker

    train_dataloader = find_data(root, batch_size = batch_size, Shuffle = False)
    trainer = Trainer(train_dataloader,TileProcessor)

    trainer.run_training(epochs)
    trainer.save_params()

    #To check how our new model weights looks, run the following code in a debug console:
    #modelherlev = torch.load("models/HerlevWeights.pth.tar", map_location=torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu"))