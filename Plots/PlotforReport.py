from src.utils.geojson_handler_utils import GeoJsonHandler

muni = "Herlev"

Municipality = GeoJsonHandler(input_dir='../municipalities.geojson', input_munic=muni)
Municipality.plot_muni()

