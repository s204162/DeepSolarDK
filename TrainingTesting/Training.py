import torch
from torch import nn
import os,sys
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE" ## Kan fjernes, men programmet virker kun for mig hvis denne er tændt og er placeret lige præcis her..
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
from torchvision import transforms, utils
from torchvision.datasets import ImageFolder
from src.pipeline_components.tile_processor import TileProcessor
from src.utils.geojson_handler_utils import GeoJsonHandler
import yaml
from torchmetrics import ConfusionMatrix
import wandb

#wandb.init(project="my-test-project", entity="radziteam")

class Trainer:
    """ This class takes a Tileprocessor object,training data. To train the model on the given training data,
    call Trainer.run_training(epochs) to display bar set bar = True"""

    def __init__(self, TileProcessor, rootpath, valpath, batch_size, lr, aux_weight, weight_decay, label_smooth,
                 optimizer = torch.optim.Adam,weights = torch.tensor([1, 5],dtype=torch.float)):
        #self.data_train, self.data_val = datatrain, dataval #Pytorch dataloader

        #Hyper params
        self.lr,self.aux_weight,self.label_smooth,self.weight_decay = lr ,aux_weight,label_smooth,weight_decay

        # Model,data og loss
        self.data_val = find_val(rootpath, batch_size)
        self.TileProcessor = TileProcessor #Give the model.
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.model = self.TileProcessor.model.to(self.device)
        self.parameters = self.model.parameters
        self.optimizer = optimizer(params = self.parameters(), lr = self.lr,weight_decay=self.weight_decay)
        self.weights = weights
        self.loss_fn = nn.CrossEntropyLoss(weight = self.weights,label_smoothing=self.label_smooth).to(self.device)  # Definer med class imbalance weights
        self.savepath = "models/"

        #Running losses
        self.loss = 0
        self.running_loss = []
        self.running_acc = []
        self.confusion_matrices = {}
        self.rootpath = rootpath
        self.validation = valpath
        self.batch_size = batch_size


    def train(self):

         #wandb.watch(self.model, self.loss_fn, log="all", log_freq=10)
         self.data_train = find_data(self.rootpath, self.batch_size, Shuffle = False)
         #Takes data as a pytorch dataloader, the model, loss and optimizer
         #self.data_train = DataLoader(self.train_data, batch_size=batch_size, shuffle = Shuffle)
         self.model.train()
         for batch, (X, y) in enumerate(self.data_train):
             X = X.to(self.device)
             y = y.to(self.device)

             #Catching errors for batching.
             try:
                yp = self.one_hot_encode(y)

                #Use softmax to get preds
                out, aux = self.model(X)
                pred,predaux = F.softmax(out, dim = 1), F.softmax(aux,dim = 1)
                loss1,loss2 = self.loss_fn(pred, yp), self.loss_fn(predaux, yp)
                self.loss = loss1+self.aux_weight*loss2 #vægtet loss mellem aux og main clasifier.

                #Back prop
                self.optimizer.zero_grad()
                self.loss.backward()
                self.optimizer.step()
                if batch % 50 == 0:
                    print(f'Batch: {batch+1}')
                    #wandb.log({"loss": self.loss})

             except:
                #Senere skriver vi til csv fil
                e = sys.exc_info()
                print(e)

             #self.confusion_matrix(pred,yp)
        #Catch errors for val_error
         #try:
         self.compute_val_error()
         print(f'Average running loss: {self.running_loss}')


    def compute_val_error(self):
        self.model.eval()
        val_loss = []
        accuracy = []
        with torch.no_grad():
            for batch, (X_val, y_val) in enumerate(self.data_val):
                X_val = X_val.to(self.device)
                y_val = y_val.to(self.device)

                #onehot encoding
                yp_val = self.one_hot_encode(y_val)

                # Loss
                logit = self.model(X_val).to(self.device)
                prob = F.softmax(logit, dim=1)
                error = self.loss_fn(prob, y_val)
                val_loss.append(error)  # skal laves til integer
                #print(f'Validation batch:{batch + 1}')
                #print("Negative:","Positive:")

                #wandb.log({"test loss": error})
                # Accuracy
                preds = torch.round(prob)
                self.confusion_matrix(preds,yp_val)
                #print(preds, y_val)
            self.Metrics()
            print(self.confusion_matrices[self.epoch_number])

    def Metrics(self):
        cf_acc = self.confusion_matrices[self.epoch_number]
        TP = cf_acc[0,0]
        TN = cf_acc[1,1]
        FP = cf_acc[0,1]
        FN = cf_acc[1,0]
        precision = TP/(TP+FP)
        recall = TP/(TP+FN)
        if TP+FP == 0 or TP+FN == 0 or precision == 0 or recall == 0:
            F1_score = torch.tensor(0)
        else:
            F1_score = 2 * (recall * precision) / (recall + precision)

        Accuracy = (TP+TN)/(TP+TN+FP+FN)
        print("prec,recall:",precision,recall)
        print(f"F1-score:{F1_score}")
        print(f"Accuracy:{Accuracy}")

    def run_training(self,epochs):
        #Kalder trænings loopet i X-antal epochs
        self.data_val = find_val(self.validation, self.batch_size, Shuffle=False)
        for t in range(epochs):
            self.epoch_number = t
            print(f'epoch {t + 1}')
            self.train()
        print("Trainning Done!")

    #Save model params
    def save_params(self):
        """ Gemmer parametre fra model til den initialiserede savepath"""

        path = os.path.join(self.savepath, 'HerlevWeights.tar')
        torch.save({'model_state_dict': self.model.state_dict()}, path)
        print(f'Saved PyTorch Model State to{self.savepath}')

    def confusion_matrix(self,prediction,ground_truth):
        """ Laver confusion matrix, fra predictions og en ground truth og printer til consol"""

        if self.epoch_number not in self.confusion_matrices.keys():
            self.confusion_matrices[self.epoch_number] = torch.zeros(2,2).to("cpu")

        # Round predictions and change type to int
        prediction,ground_truth = torch.round(prediction).to(torch.int).to("cpu"), ground_truth.to(torch.int).to("cpu")
        prediction,ground_truth = self.decode(prediction).to(torch.int).to("cpu"), self.decode(ground_truth).to(torch.int).to("cpu")

        #Create confusion matric object, and call it.
        confmat = ConfusionMatrix(num_classes = 2, multilabel=False)
        cf = confmat(prediction, ground_truth).to("cpu")
        self.confusion_matrices[self.epoch_number] += cf.to(torch.int) #Increment confusion matrix


    def one_hot_encode(self,y_val):
        """ One hot encoder binære labels, og returner en tensor med de encodede labels"""
        yp_val = torch.zeros(len(y_val), 2).to(self.device)
        for u in range(len(y_val)):
            if y_val[u] == 1:
                yp_val[u, :] = torch.tensor([0, 1], dtype=torch.float64).to(self.device)
            if y_val[u] == 0:
                yp_val[u, :] = torch.tensor([1, 0], dtype=torch.float64).to(self.device)
        return yp_val

    def decode(self,labels):
        yp_val = torch.zeros(len(labels)).to(self.device)
        for u in range(len(labels)):
            if labels[u,0] == 1:
                yp_val[u] = 1
            if labels[u,0] == 0:
                yp_val[u] = 0
        return yp_val



# Load images and splits them into train and validation, and turns them into dataloaders
def find_data(rootpath, batch_size, Shuffle = False):
    train_size = 0.8 # Defines the procentage of our train split.
    trans = transforms.Compose([
        transforms.Resize(299),
        transforms.ToTensor(),
        transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.RandomVerticalFlip(p=0.5),
        # Choosing saturation value uniformly in the range 0-1:
        transforms.ColorJitter(saturation=(0.1, 1.9))  # brightness=0, contrast=0, saturation=(0,2), hue=0
        #transforms.RandomRotation(degrees=90)
        #transforms.functional.adjust_saturation(saturation_factor=4)
    ])

    train_data = ImageFolder(root=rootpath,transform=trans)

    #train_data, _ = torch.utils.data.random_split(dataset, [int(train_size*len(dataset)), len(dataset)-int(train_size*len(dataset))])

    train_data = DataLoader(train_data, batch_size=batch_size, shuffle = Shuffle)

    print("New dataloader", train_data)
    return train_data

def find_val(validation, batch_size, Shuffle = False):
    trans = transforms.Compose([
        transforms.Resize(299),
        transforms.ToTensor(),
        transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
    ])

    dataset = ImageFolder(root=validation, transform=trans)

    #train_data = DataLoader(train_data, batch_size=batch_size, shuffle = Shuffle)
    val_data = DataLoader(dataset, batch_size=batch_size, shuffle=Shuffle)

    return val_data


# Soon to be deleted, since we will run training through run_pipeline
if __name__ == '__main__':
    # ------- Read configuration -------

    config_file = 'config.yml'

    with open(config_file, 'rb') as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)

    run_tile_processor = False

    tile_coords_path = conf.get('tile_coords_path', 'data/coords/TileCoords.pickle')
    geojson_path = conf.get('geojson_path', "municipalities.geojson")
                            #'utils/deutschlandGeoJSON/2_bundeslaender/1_sehr_hoch.geo.json')
    downloaded_path = conf.get('downloaded_path', 'logs/processing/DownloadedTiles.csv')
    processed_path = conf.get('processed_path', 'logs/processing/Processed.csv')

    municipality = 'Herlev'  # HER er ændret

    # ------- GeoJsonHandler provides utility functions -------

    municipality_handler = GeoJsonHandler(geojson_path, municipality)  # HER er ændret

    #lr, aux_weight, weight_decay, label_smooth
    #Train model on a dataset from "root" location.
    batch_size = 32
    epochs = 15
    lr = 1e-4
    aux_weight = 0.1
    weight_decay = 0.0001
    label_smooth = 0.10
    ClassWeights = torch.tensor([1, 10], dtype=torch.float)


    # wandb.config = {
    #     "learning_rate": lr,
    #     "epochs": epochs,
    #     "batch_size": batch_size,
    #     "aux_weight": aux_weight,
    #     "weight_decay": weight_decay,
    #     "label_smooth": label_smooth,
    #     "classweights": ClassWeights
    # }


    tileProcessor = TileProcessor(configuration = conf, polygon=municipality_handler.polygon)

    root = "data/splittiles_labeled/" #Kun et testset for at se om programmet virker
    valset = "data/valdata"
    # root = "data/testdata/" #Kun et testset for at se om programmet virker
    # valset = "data/testdata/"
    #training, validation = find_data(root, batch_size = batch_size, Shuffle = True)
    trainer = Trainer(tileProcessor, rootpath=root, valpath = valset, batch_size=batch_size, lr = lr, aux_weight=aux_weight, weight_decay=weight_decay, label_smooth=label_smooth, weights=ClassWeights)

    trainer.run_training(epochs)
    #trainer.save_params()

    #Til at sammenligne weights på herlev og pipeline
    #modelhvor = "models/HerlevWeights.tar"
    #modelhvor2 = "models/inceptionv3_weights.tar"
    #check = torch.load(modelhvor, map_location=torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu"))
    #check2 = torch.load(modelhvor, map_location=torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu"))


    #To check how our new model weights looks, run the following code in a debug console:
    #modelherlev = torch.load("models/HerlevWeights.pth.tar", map_location=torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu"))