import torch
from torchvision.models import Inception3
import random
from torch import nn
import os, sys
import time
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torchvision.datasets import ImageFolder
from torchmetrics import ConfusionMatrix, CohenKappa
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import wandb
from sklearn.metrics import f1_score
import plotly.express as px
import csv
import numpy as np
import torch.nn.functional as F

# SEEDS:
torch.manual_seed(420)
random.seed(420)


# Initialize our trainer class:
class Tester(object):

    def __init__(self, batchsize, datapath, modelpath, label_smooth, weights):
        self.device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        self.modelpath = modelpath
        self.model = self.loadModel()
        self.batchsize = batchsize
        self.datapath = datapath
        self.criterion = nn.CrossEntropyLoss(weight=weights, label_smoothing=label_smooth).to(self.device)
        self.dataset, self.dataloaders = self.dataCreator()

    # Specify model architecture
    def loadModel(self):
        model = Inception3(num_classes=2, aux_logits=True, transform_input=False)
        model = model.to(self.device)
        checkpoint = torch.load(self.modelpath, map_location=self.device)
        model.load_state_dict(checkpoint['model_state_dict'])

        return model

    def dataCreator(self):
        # Transformations for our test data:
        data_transforms = {'test': transforms.Compose([
                transforms.Resize(299),
                transforms.ToTensor(),
                transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            ]),
        }

        dataset = {'test': ImageFolder(os.path.join(self.datapath, 'test'), data_transforms['test'])}
        dataloaders_dict = {'test': DataLoader(dataset['test'], batch_size=self.batchsize, shuffle=False)}

        return [dataset,dataloaders_dict]


def test_model(TesterClass, model, modelpath, dataset, dataloaders, criterion, is_inception=False):

    device = TesterClass.device
    since = time.time()
    cf = ConfusionMatrix(num_classes=2)
    ck = CohenKappa(num_classes = 2)
    cfs = {}


    wandb.watch(model,criterion, log='all',log_freq=10)

    model.eval()  # Set model to evaluate mode
    cfs['test'] = torch.zeros(2, 2).to("cpu")

    running_loss = 0.0
    running_corrects = 0
    running_probs = []
    running_predicts = []
    running_labels = []

    # Iterate over data.
    for inputs, labels in dataloaders['test']:
        with torch.no_grad():
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            probs = F.softmax(outputs, dim=1)
            loss = criterion(outputs, labels)

            _, preds = torch.max(outputs, 1)

        running_probs.append(probs.to("cpu"))
        running_predicts.append(preds.to("cpu"))
        running_labels.append(labels.to("cpu"))

        # statistics
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)
        cfs['test'] += cf(labels.to("cpu"), preds.to("cpu"))


    running_predicts = torch.cat(running_predicts).numpy()
    running_labels = torch.cat(running_labels).numpy()
    running_probs = torch.cat(running_probs).numpy()
    loss = running_loss / len(dataloaders['test'].dataset)
    f1 = f1_score(running_labels, running_predicts, zero_division=0)


    # Plot is confusionmatrix and other metrics
    fig = px.imshow(cfs['test'], text_auto=True,
                    labels=dict(x="Predicted", y="Ground truth", color="Productivity"),
                    x=['Negative', 'Positive'],
                    y=['Negative', 'Positive'])
    cohen = ck(torch.tensor(running_predicts), torch.tensor(running_labels))
    precision = precision_score(running_predicts, running_labels)
    recall = recall_score(running_predicts, running_labels)

    # Get plots of wrongly classified images!
    #
    # Indices with wrong predictions
    false_instances = running_predicts != running_labels
    where_1 = running_labels == 1
    where_0 = running_labels == 0
    FN = np.where(np.logical_and(false_instances, where_1))[0]
    FP = np.where(np.logical_and(false_instances, where_0))[0]
    # Take two randoms
    a, b = np.random.choice(FN, size=2, replace = False), np.random.choice(FP, size = 2, replace = False)
    # Take two with high confidence
    c, d = np.argsort(running_predicts[np.logical_and(false_instances, where_1)])[-2:], np.argsort(running_predicts[np.logical_and(false_instances, where_0)])[-2:]
    image_list = {}
    all_index = np.concatenate([a, b, c, d], axis=0)
    for i in all_index:
        (input, label) = dataset['test'][i]
        # Inverse transform image
        inv_normalize = transforms.Compose([
        transforms.Normalize([-0.5/0.5, -0.5/0.5, -0.5/0.5], [1/0.5, 1/0.5, 1/0.5]),
        ])
        image = inv_normalize(input)
        image = image.numpy().transpose(1, 2, 0)
        image_list[i] = image

    FN0_image = px.imshow(image_list[a[0]])
    FP0_image = px.imshow(image_list[b[0]])
    FN1_image = px.imshow(image_list[a[1]])
    FP1_image = px.imshow(image_list[b[1]])
    FN2_image = px.imshow(image_list[c[0]])
    FP2_image = px.imshow(image_list[d[0]])
    FN3_image = px.imshow(image_list[c[1]])
    FP3_image = px.imshow(image_list[d[1]])

    wandb.log({"test_loss": loss,
               "F1Score_test": f1,
               "conf_mat": wandb.data_types.Plotly(fig),
               "Rand1 - Predicted:0 - Label:1": wandb.data_types.Plotly(FN0_image),
               "Rand1 - Predicted:1 - Label:0": wandb.data_types.Plotly(FP0_image),
               "Rand2 - Predicted:0 - Label:1": wandb.data_types.Plotly(FN1_image),
               "Rand2 - Predicted:1 - Label:0": wandb.data_types.Plotly(FP1_image),
               "High1 - Predicted:0 - Label:1": wandb.data_types.Plotly(FN2_image),
               "High1 - Predicted:1 - Label:0": wandb.data_types.Plotly(FP2_image),
               "High2 - Predicted:0 - Label:1": wandb.data_types.Plotly(FN3_image),
               "High2 - Predicted:1 - Label:0": wandb.data_types.Plotly(FP3_image),
               "CohenKappa": cohen,
               "Precision": precision,
               "Recall": recall})

    print(cfs['test'])
    print('{} Loss: {:.4f} F1: {:.4f}'.format('Test', loss, f1))

    print()

    time_elapsed = time.time() - since
    print('Testing complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))


    # Write to CSV
    data = []
    [data.append([nega, posi, pred, label]) for (nega, posi, pred, label) in zip(running_probs[0], running_probs[1], running_predicts, running_labels)]
    stringname = 'PredTesting' + modelpath[7:-4] + '.csv'
    file = open(stringname, 'w+', newline='')
    with file:
        write = csv.writer(file)
        write.writerow(['Nega', 'Posi', 'Predictions', 'TrueLabels'])
        write.writerows(data)


    # Return preditions and labels
    return running_predicts, running_labels


if __name__ == '__main__':
    datapath = 'data/gentofte_trainval/'
    modelpath = 'models/inceptionv3_weights.tar'
    batch_size = 32
    label_smooth = 0.1
    ClassWeights = torch.tensor([1, 5], dtype=torch.float)

    run = wandb.init(project="DeepSolarDK", entity="radziteam", config={
        "batch_size": batch_size,
        "label_smooth": label_smooth,
        "classweights": ClassWeights,
    })

    OPModel = Tester(batchsize=batch_size, datapath=datapath, modelpath=modelpath, label_smooth=label_smooth, weights=ClassWeights)

    predictions, labels = test_model(TesterClass= OPModel, model = OPModel.model, modelpath = modelpath, dataset=OPModel.dataset,
                                    dataloaders = OPModel.dataloaders, criterion= OPModel.criterion, is_inception = True)


    run.finish()