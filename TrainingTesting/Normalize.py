from torchvision import datasets
from torchvision import transforms
from torchvision.datasets import ImageFolder
import torch
from torch.utils.data import DataLoader

root = 'data/datatest'

trans = transforms.Compose([
        transforms.Resize(299),
        transforms.ToTensor(),
    ])

dataset = ImageFolder(root=root, transform=trans)

img_t, index_label = dataset[5]
type(img_t), type(index_label)

imgs = torch.stack([img_t for img_t, _ in dataset], dim=3)
imgs.shape

print('Mean:', imgs.view(3, -1).mean(dim=1))
print('Mean:',imgs.view(3, -1).std(dim=1))