import numpy as np
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder
import torch
from torchvision import transforms
import scipy.stats as st
import scipy
import pandas as pd
import plotly.express as px
import plotly.io as pio
import seaborn as sns
from matplotlib import pyplot as plt
import plotly.graph_objects as go
from torchvision.models import Inception3, inception_v3

def expected_solar_deployment(N,n1,precision,recall):
    """ Function calculating the expected amount of solar panels deployed, based on:
    N= number of images
    n1 = Number of positive predictions.
    precission from test set
    recall from test set
    See eq. 12 and 13"""

    n0 = N-n1
    TP = n1*precision
    FP = n1-TP
    FN = TP/recall-TP
    TN = n0-FN
    p11 = TP/(TP+FP)
    p10 = FN/(FN+TN)
    expected = n1*p11+n0*p10
    sigma = np.sqrt(n1*p11*(1-p11)+n0*p10*(1-p10))

    return expected,sigma

def std_mean(dataloader):
    imgs = torch.stack([img_t for img_t, _ in Dl], dim=3)
    mean = imgs.view(3, -1).mean(dim=1)
    std = imgs.view(3, -1).std(dim=1)

    return mean, std

def mcnemar(y_true, yhatA, yhatB, alpha=0.05):
    """ Code from the toolBox from course 02450 (Introduction to Machine Learning and Data mining """
    # perform McNemars test
    nn = np.zeros((2,2))
    c1 = yhatA - y_true == 0
    c2 = yhatB - y_true == 0

    nn[0,0] = sum(c1 & c2)
    nn[0,1] = sum(c1 & ~c2)
    nn[1,0] = sum(~c1 & c2)
    nn[1,1] = sum(~c1 & ~c2)

    n = sum(nn.flat);
    n12 = nn[0,1]
    n21 = nn[1,0]

    thetahat = (n12-n21)/n
    Etheta = thetahat

    Q = n**2 * (n+1) * (Etheta+1) * (1-Etheta) / ( (n*(n12+n21) - (n12-n21)**2) )

    p = (Etheta + 1)*0.5 * (Q-1)
    q = (1-Etheta)*0.5 * (Q-1)

    CI = tuple(lm * 2 - 1 for lm in scipy.stats.beta.interval(1-alpha, a=p, b=q) )

    p = 2*scipy.stats.binom.cdf(min([n12,n21]), n=n12+n21, p=0.5)
    print("Result of McNemars test using alpha=", alpha)
    print("Comparison matrix n")
    print(nn)
    if n12+n21 <= 10:
        print("Warning, n12+n21 is low: n12+n21=",(n12+n21))

    print(f"Approximate 1-alpha confidence interval of theta: [thetaL,thetaU] ={CI}")
    print(f"p-value for two-sided test A and B have same accuracy (exact binomial test): p={p}")

    return thetahat, CI, p

def run_mcnemar(csv1_path,csv2_path):
    """Tager to csv'er med labels of ground truth og kører McNemars test. Csv collone format: [Predictions,TrueLabels] """
    C1, C2 = pd.read_csv(csv1_path),pd.read_csv(csv2_path)
    if (C1['TrueLabels'].to_numpy() != C2['TrueLabels'].to_numpy()).all():
        print("McNemar skal evalueres på samme test set")
        return None
    y_true,C1_pred,C2_pred = C1['TrueLabels'].to_numpy(),C1['Predictions'].to_numpy(),C2['Predictions'].to_numpy()
    return mcnemar(y_true,C1_pred,C2_pred)


def plot_distribution(path1,path2,name):
    df1 = pd.read_csv(f'CsvFilerTilPlots/{path1}')
    df2 = pd.read_csv(f'CsvFilerTilPlots/{path2}')
    postive_probs1, negative_probs1 =  df1['Posi'], df1['Nega']
    postive_probs2, negative_probs2 = df2['Posi'], df2['Nega']

    #
    mask11 = df1['Posi'] > 0.5
    mask12 = df1['Nega'] > 0.5
    positive1, negative1 = df1[mask11], df1[mask12]

    mask21 = df2['Posi'] > 0.5
    mask22 = df2['Nega'] > 0.5
    positive2, negative2 = df2[mask21], df2[mask22]

    #pos plot
    fig, ax = plt.subplots()
    for a in [positive1['Posi'], positive2['Posi']]:
        sns.distplot(a, ax=ax, kde=False, bins = 25)
    #ax.set_xlim([0, 100])
    plt.title('Positive distribution')
    plt.xlabel('Positive probabillity')
    fig.legend(labels=['DeepSolarDK','DeepSolarGermany'],loc=10)
    fig.savefig(f"CsvFilerTilPlots/positive{name}")

    #Neg plot
    fig, ax = plt.subplots()
    for a in [negative1['Nega'], negative2['Nega']]:
        sns.distplot(a, ax=ax, kde=False, bins = 25)
    # ax.set_xlim([0, 100])
    plt.title('Negative distribution')
    plt.xlabel('Negative probabillity')
    fig.legend(labels=['DeepSolarDK', 'DeepSolarGermany'],loc=10)
    fig.savefig(f"CsvFilerTilPlots/negative{name}")

    ##figP = px.histogram(positive1, 'Posi', nbins = 35)
    #figP.up
    #figP.update_layout(title='Distribution of positive probs', xaxis_title="Probabillity", yaxis_title="Count")
    #figN = px.histogram(negative,'Nega')
    #figN.update_layout(title='Distribution of negative probs', xaxis_title="Probabillity", yaxis_title="Count")


    #pio.write_image(figP, f"CsvFilerTilPlots/positive{name}.png")
    #pio.write_image(figN, f"CsvFilerTilPlots/negative{name}.png")

if __name__ == '__main__':

    #Syddanmark
#    antal ,afvigelse = expected_solar_deployment(N= 225*35423, n1 = 20021, precision = 0.65, recall = 0.73)
 #   print('Syddanmark',antal, afvigelse)

#    #Hovedstaden
  #  antal, afvigelse = expected_solar_deployment(N=225*7594, n1=15102, precision=0.65, recall=0.73)
 #   print('Hovedstaden',antal, afvigelse)

    #Nordjylland
   # antal, afvigelse = expected_solar_deployment(N=225*23007, n1=10192, precision=0.65, recall=0.73)
    #print('Nordjylland',antal, afvigelse)

    #MidtJylland
    #antal, afvigelse = expected_solar_deployment(N=225*37804, n1=17118, precision=0.65, recall=0.73)
    #print('MidtJylland',antal, afvigelse)

    #Sjælland
    #antal, afvigelse = expected_solar_deployment(N=225*21665, n1=11168, precision=0.65, recall=0.73)
    #print('Sjælland',antal, afvigelse)
    total = 21665 + 37804 +23007 +7594 +35423
    antal, afvigelse = expected_solar_deployment(N=225 * total, n1=73599, precision=0.65, recall=0.73)
    print('Total:', antal, afvigelse)
    #model = inception_v3(pretrained=True, aux_logits=True, transform_input=False)
    #plot_distribution('PredTestingFinalWeights_GermanConfig.csv','PredTestinginceptionv3_weights.csv','TyskeVægt')

    #csvpath1 = 'CsvMcNemar/PredTestingGentofteTrainVal.csv'
    #csvpath2 = 'CsvMcNemar/PredTestingGentofte128.csv'
    #csvpath2 = 'CsvMcNemar/PredTestingFinalWeights_GermanConfig.csv'
    #thetahat, CI, p = run_mcnemar(csvpath1,csvpath2)
    #print(CI)
   # class_sample_count = [4701, 290]
    #weights = 1 / torch.Tensor(class_sample_count)
    #weights = weights.double()
    #sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, 8)

    #phase = 'train'

   # transform = transforms.Compose([transforms.Resize(299), transforms.ToTensor()])
   # data = ImageFolder('data/splittiles_labeled/test', transform= transform)
   # Dl = DataLoader(data,sampler = sampler if phase == 'train' else None)

   # mean, std = std_mean(Dl)
   # print(mean,std)

    #Train herlev
    #tensor([0.4678, 0.4682, 0.4391]) tensor([0.1684, 0.1568, 0.1514])

    #Test herlev
