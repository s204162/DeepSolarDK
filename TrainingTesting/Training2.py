import torch
from torchvision.models import Inception3, inception_v3
import random
from torch import nn
import os, sys
import time
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torchvision.datasets import ImageFolder
import copy
from torchmetrics import ConfusionMatrix, F1Score, CohenKappa
import wandb
from sklearn.metrics import f1_score, precision_score, recall_score
import plotly.express as px
from torch.optim.lr_scheduler import ExponentialLR, StepLR

# SEEDS:
torch.manual_seed(420)
random.seed(420)


# Initialize our trainer class:
class Trainer(object):

    def __init__(self, optimizer, momentum, batchsize, epochs, datapath, lr, weight_decay, label_smooth, weights):
        self.device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        self.model = self.loadModel()
        self.batchsize = batchsize
        self.epochs = epochs
        self.datapath = datapath
        # Set optimizer
        if optimizer == 'SGD':
            self.optimizer = torch.optim.SGD(self.model.parameters(), lr=lr, momentum=momentum,
                                             weight_decay=weight_decay)
        else:  # Adam is default
            self.optimizer = torch.optim.Adam(self.model.parameters(), lr=lr, weight_decay=weight_decay)
        # self.optimizer = torch.optim.SGD(self.model.parameters(), lr=lr, momentum=0.9)
        self.criterion = nn.CrossEntropyLoss(weight=weights, label_smoothing=label_smooth).to(self.device)
        self.dataloaders = self.dataCreator()

    # Specify model architecture
    def loadModel(self):
        #model = Inception3(num_classes=2, aux_logits=True, transform_input=False,)
        model = inception_v3(pretrained=True, aux_logits=True, transform_input=False)
        model.fc = nn.Linear(2048, 2)
        model = model.to(self.device)
        #checkpoint = torch.load('models/inceptionv3_weights.tar', map_location=self.device)
        #model.load_state_dict(checkpoint['model_state_dict'])
        return model

    def dataCreator(self):
        # Transformations for our train/val data:
        data_transforms = {
            'train': transforms.Compose([
                transforms.Resize(299),
                transforms.ToTensor(),
                transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
                transforms.RandomHorizontalFlip(p=0.5),
                transforms.RandomVerticalFlip(p=0.5),
                # Choosing saturation value uniformly in the range 0-1:
                #transforms.ColorJitter(saturation=(0.1, 1.9))  # brightness=0, contrast=0, saturation=(0,2), hue=0
            ]),
            'val': transforms.Compose([
                transforms.Resize(299),
                transforms.ToTensor(),
                transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            ]),
        }

        # Prøv med bootstrap
        # class_sample_count = [4701,290]
        # weights = 1 / torch.Tensor(class_sample_count)
        # weights = weights.double()
        # sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, self.batchsize)
        # sampler= sampler if x == 'train' else None
        datasets = {x: ImageFolder(os.path.join(self.datapath, x), data_transforms[x]) for x in ['train', 'val']}
        dataloaders_dict = {x: DataLoader(datasets[x], batch_size=self.batchsize, shuffle=True) for x in
                            ['train', 'val']}

        return dataloaders_dict


def train_model(aux_weight, TrainerClass, model, dataloaders, criterion, optimizer, lr_decrease, num_epochs=25,
                is_inception=False):
    device = TrainerClass.device
    since = time.time()
    cf = ConfusionMatrix(num_classes=2)
    ck = CohenKappa(num_classes=2)
    cfs = {}
    current_best_kappa = [0, 0]  # epoch idx0 kappa idx1
    val_f1_history = []

    best_model_wts = copy.deepcopy(model.state_dict())
    best_f1 = 0.0

    scheduler = StepLR(optimizer, step_size=5, gamma=lr_decrease)
    wandb.watch(model, criterion, log='all', log_freq=10)
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)
        cfs[epoch] = {}
        i = 0
        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode
            i += 1
            cfs[epoch][phase] = torch.zeros(2, 2).to("cpu")

            running_loss = 0.0
            running_corrects = 0
            running_predicts = []
            running_labels = []

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)
                i += 1

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    # Special case for inception because in training it has an auxiliary output. In train
                    #   mode we calculate the loss by summing the final output and the auxiliary output
                    #   but in testing we only consider the final output.
                    if is_inception and phase == 'train':
                        # From https://discuss.pytorch.org/t/how-to-optimize-inception-model-with-auxiliary-classifiers/7958
                        outputs, aux_outputs = model(inputs)
                        loss1 = criterion(outputs, labels)
                        loss2 = criterion(aux_outputs, labels)
                        loss = loss1 + aux_weight * loss2
                    else:
                        outputs = model(inputs)
                        loss = criterion(outputs, labels)

                    _, preds = torch.max(outputs, 1)

                    running_predicts.append(preds.to("cpu"))
                    running_labels.append(labels.to("cpu"))

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
                cfs[epoch][phase] += cf(preds.to("cpu"), labels.to("cpu"))

                # print for every 15th batch:
                if i % 15 == 0:
                    print(cfs[epoch][phase])
                    print(running_corrects)

            running_predicts = torch.cat(running_predicts).numpy()
            running_labels = torch.cat(running_labels).numpy()
            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_f1 = f1_score(running_labels, running_predicts, zero_division=0)
            # epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

            # fig.show()
            if phase == 'train':
                wandb.log({"train_epoch_loss": epoch_loss,
                           "train_epoch_f1": epoch_f1})

            else:
                fig = px.imshow(cfs[epoch][phase], text_auto=True,
                                labels=dict(x="Predicted", y="Ground truth", color="Productivity"),
                                x=['Negative', 'Positive'],
                                y=['Negative', 'Positive'])
                fig.update_coloraxes(showscale=False)
                cohen = ck(torch.tensor(running_predicts), torch.tensor(running_labels))
                precision = precision_score(running_predicts, running_labels)
                recall = recall_score(running_predicts, running_labels)
                wandb.log({"val_epoch_loss": epoch_loss,
                           "F1Score_val": epoch_f1,
                           "conf_mat": wandb.data_types.Plotly(fig),
                           "CohenKappa": cohen,
                           "Precision": precision,
                           "Recall": recall})
                # early stopping
                if cohen > current_best_kappa[1]:
                    current_best_kappa[0], current_best_kappa[1] = epoch, cohen

            print(cfs[epoch][phase])
            print('{} Loss: {:.4f} F1: {:.4f}'.format(phase, epoch_loss, epoch_f1))

            # deep copy the models
            if phase == 'val' and epoch_f1 > best_f1:
                best_f1 = epoch_f1
                best_model_wts = copy.deepcopy(model.state_dict())
                best_epoch_number = epoch
            if phase == 'val':
                val_f1_history.append(epoch_f1)

        print()
        scheduler.step()
        # if we havent improved for 10 epochs break the loop
        if epoch - current_best_kappa[0] > 15:
            break

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val f1: {:4f}'.format(best_f1))

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model, val_f1_history, cfs, best_epoch_number


def save_params(model, path, datapath):
    """ Gemmer parametre fra model til den initialiserede savepath"""

    path = os.path.join(path, f'FinalWeights_{datapath}.tar')
    torch.save({'model_state_dict': model.state_dict()}, path)


if __name__ == '__main__':
    datapath = 'data/GentofteTrainVal'
    batch_size = 128
    epochs = 50
    lr = 1e-3
    lr_decrease = 0.2
    weight_decay = 0.0001
    label_smooth = 0.075
    ClassWeights = torch.tensor([1, 5], dtype=torch.float)
    aux_weight = 0.1
    optimizer = 'adam'
    momentum = 0.5  # only used if optimizer = 'SGD'

    run = wandb.init(project="DeepSolarDK", entity="radziteam", config={
        "learning_rate": lr,
        "lr_decrease": lr_decrease,
        "epochs": epochs,
        "batch_size": batch_size,
        "weight_decay": weight_decay,
        "label_smooth": label_smooth,
        "classweights": ClassWeights,
        "aux_weight": aux_weight,
        "datapath": datapath
    })

    # optimizer,momentum,batchsize, epochs, datapath, lr, weight_decay, label_smooth, weights
    OPModel = Trainer(optimizer, momentum, batch_size, epochs, datapath, lr, weight_decay, label_smooth, ClassWeights)

    model_ft, hist, cfs, best_epoch_number = train_model(aux_weight, OPModel, OPModel.model, OPModel.dataloaders,
                                                         OPModel.criterion,
                                                         OPModel.optimizer, lr_decrease, epochs, True)

    save_params(model_ft, 'models/', datapath)

    run.finish()
