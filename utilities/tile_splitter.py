# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 14:31:47 2019

@author: Kevin
"""
from __future__ import print_function
from __future__ import division
import yaml
from tqdm import tqdm
import os
import numpy as np
from itertools import compress
from shapely.geometry import Point
from PIL import Image
import cv2

from src.utils.geojson_handler_utils import GeoJsonHandler


folder_dir = "data/tiles"
save_dir = "data/HER"
config_file = 'config.yml'

with open(config_file, 'rb') as f:

    conf = yaml.load(f, Loader=yaml.FullLoader)

tile_coords_path = conf.get('tile_coords_path', 'data/coords/TileCoords.pickle')
geojson_path = conf.get('geojson_path', 'utils/deutschlandGeoJSON/2_bundeslaender/1_sehr_hoch.geo.json')
downloaded_path = conf.get('downloaded_path', 'logs/processing/DownloadedTiles.csv')
processed_path = conf.get('processed_path', 'logs/processing/Processed.csv')

municipality = 'Gentofte'
# ------- GeoJsonHandler provides utility functions -------

municipality_handler = GeoJsonHandler(geojson_path, municipality)
polygon = municipality_handler.polygon

# Avg. earth radius in meters
radius = 6371000

# Square side length in meters
side = 40

# dlat spans a distance of 40 meters in north-south direction:
dlat = (side * 360) / (2 * np.pi * radius)

def splitTile(tile, minx, miny, maxx, maxy):

    minx = float(minx)
    miny = float(miny)
    maxx = float(maxx)
    maxy = float(maxy)

    # Takes a 4800x4800 image tile and returns a list of 320x320 pixel images, if they are within
    # the municipality polygon

    tile = np.array(tile)

    images = []
    coords = []

    N = 0
    S = 4800
    W = 0
    E = 4800

    # The first image is taken from the upper left corner, we then slide from left
    # to right and from top to bottom. Since our images shall cover 40x40m, the initial
    # y coordinate is dlat/2 degrees, i.e. 20 meters, south of the maximum y coordinate.
    y_coord = maxy - dlat/2

    while N < S:

        W = 0

        x_coord = minx + (((side * 360) / (2 * np.pi * radius * np.cos(np.deg2rad(maxy))))/2)

        while W < E:

            # The first image is taken from the upper left corner, we then slide from left
            # to right and from top to bottom

            images.append(tile[N:N+320, W:W+320])
            coords.append((x_coord, y_coord))

            x_coord += (((side * 360) / (2 * np.pi * radius * np.cos(np.deg2rad(y_coord)))))

            W = W + 320

        N = N + 320

        y_coord = y_coord - dlat

    # A boolean vector of length 225 indicating whehter a image's centerpoint is within the municipality polygon
    #coords_boolean = [self.polygon.intersects(Point(elem)) for elem in coords]
    coords_boolean = [polygon.intersects(Point(elem)) for elem in coords]

    # A list containing all images from the current tile that lie within NRW
    imagesInNRW = list(compress(images, coords_boolean))
    coordsInNRW = list(compress(coords, coords_boolean))

    return coordsInNRW, imagesInNRW

def processTiles(currentTile, NoOfTiles):

    # Load image tile
    tile = Image.open(folder_dir + "/" + currentTile)
    # tile = cv2.imread(folder_dir + "/" + currentTile)

    if not tile.mode == 'RGB':

        tile = tile.convert('RGB')


    currentTile = currentTile[:-13]

    minx, miny, maxx, maxy = currentTile.split(',')

    coords, images = splitTile(tile, minx, miny, maxx, maxy)

    length = len(images)

    NoOfTiles += length

    print(f"There are {len(images)} images in this tile \n")

    for i, img in enumerate(images):

        Image.fromarray(img).save(save_dir + "/" + currentTile + "_" + str(i) + ".png")
        #cv2.imwrite(save_dir + "/" + currentTile + "_" + str(i) + ".png", img)

    return NoOfTiles

def main():
    # Get path for tiles:
    allImgs = np.array(os.listdir(folder_dir))

    print(f"There are {len(allImgs)} tiles to be split")

    NoOfTiles = 0

    # Get tiles in path:
    for Tile in tqdm(os.listdir(folder_dir)):
        NoOfTiles = processTiles(currentTile=Tile, NoOfTiles=NoOfTiles)

    print(f"{NoOfTiles} images have been created from {len(allImgs)} tiles.")

if __name__ == "__main__":
    main()