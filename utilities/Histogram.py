from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division
import seaborn as sns
from src.pipeline_components.tile_processor import TileProcessor
import numpy as np
from pathlib import Path
import torch
from torchvision import datasets, models, transforms, utils
import time
import csv
import os
import numpy as np
from itertools import compress
from shapely.geometry import Point
from PIL import Image
from torch.nn import functional as F
from torchvision.models import Inception3
from torch.utils.data import Dataset, DataLoader
from src.dataset.dataset import NrwDataset
import sys
from src.utils.geojson_handler_utils import GeoJsonHandler
from src.pipeline_components.tile_updater import TileCoordsUpdater
import yaml
from pathlib import Path
import os
import pandas as pd
from src.pipeline_components.tile_creator import TileCreator
from src.pipeline_components.tile_downloader import TileDownloader
from src.pipeline_components.tile_processor import TileProcessor
from src.utils.geojson_handler_utils import GeoJsonHandler

class TileProcessorWithHist(TileProcessor):
    def __init__(self,configuration,polygon):
        super().__init__(configuration,polygon)
        self.probs = []

    def processTiles(self, currentTile, trans):
        # Load image tile
        tile = Image.open(Path(self.tile_dir + "/" + currentTile))
        if not tile.mode == 'RGB':
            tile = tile.convert('RGB')
        print(tile.size)
        currentTile = currentTile[:-13]
        minx, miny, maxx, maxy = currentTile.split(',')
        coords, images = self.splitTile(tile, minx, miny, maxx, maxy)
        length = len(images)
        if length == 0:
            pass
        else:
            k = int(length / self.batch_size)
            for i in range(k):
                print("i: ", i, "k:", k)
                images_sub = images[self.batch_size * i:self.batch_size * (i + 1)]
                coords_sub = coords[self.batch_size * i:self.batch_size * (i + 1)]

                # converting our image tensor from [3,299,299] to [1,3,299,299]
                images_sub = [torch.unsqueeze(trans(Image.fromarray(image)), 0) for image in images_sub]
                images_sub = torch.cat(images_sub, dim=0)

                # Classify image
                outputs = self.model(images_sub.to(self.device))
                prob = F.softmax(outputs, dim=1)
                prob = prob.cpu().detach().numpy()
                self.probs.append(prob)

                # If classification is positive, save coordinates x,y into database
                print(prob)
                PV_bool = prob[:, 1] >= self.threshold
                PV_coords = list(compress(coords_sub, PV_bool))

                if PV_coords != []:
                    with open(Path(self.pv_db_path), "a") as csvFile:  # HERHERHHERHERHERHER CARL
                        # CHECK DEN DER .pv_db_path -wrong!
                        writer = csv.writer(csvFile, lineterminator="\n")
                        for elem in range(len(PV_coords)):
                            writer.writerow([PV_coords[elem]])

            #Hvorfor gør vi det to gange???
            images_sub = images[self.batch_size * k:length]
            coords_sub = coords[self.batch_size * k:length]
            images_sub = [torch.unsqueeze(trans(Image.fromarray(image)), 0) for image in images_sub]
            images_sub = torch.cat(images_sub, dim=0)

            # Classify image
            outputs = self.model(images_sub.to(self.device))
            prob = F.softmax(outputs, dim=1)
            prob = prob.cpu().detach().numpy()
            self.probs.append(prob)

            # If classification is positive, save coordinates x,y into database
            PV_bool = prob[:, 1] >= self.threshold
            PV_coords = list(compress(coords_sub, PV_bool))
            print(PV_coords)

            if PV_coords != []:
                with open(Path(self.pv_db_path), "a") as csvFile:
                    writer = csv.writer(csvFile, lineterminator="\n")
                    for elem in range(len(PV_coords)):
                        writer.writerow([PV_coords[elem]])

    def run(self):

        print('Dataset Size:', len(self.dataset))

        dataloader = DataLoader(self.dataset, batch_size=1, num_workers=0)

        self.model.eval()

        trans = transforms.Compose([
            transforms.Resize(self.input_size),
            transforms.ToTensor(),
            transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
        ])

        for i, batch in enumerate(dataloader):
            print(f'Super{i}')
            if i > 3:
                break
            currentTile = batch[0]

            # Try to process and record it
            try:

                self.processTiles(currentTile, trans)

                with open(Path(self.processed_path), "a") as csvFile:

                    writer = csv.writer(csvFile, lineterminator="\n")

                    writer.writerow([currentTile])

            # Only tiles that weren't fully processed are saved subsequently
            # ToDo: Catch the exception and write it in a second column in the .csv
            except:

                e = sys.exc_info()
                print(e)
                # Save the tile which could not be processed and continue
                with open(Path(self.not_processed_path), "a") as csvFile:

                    writer = csv.writer(csvFile, lineterminator="\n")

                    writer.writerow([currentTile, e])

            # Delete iterated tile
            #os.remove(Path(self.tile_dir + "/" + str(currentTile)))


    def plot_distribution(self, bins = 30, alpha = 0.5):
        proba = np.concatenate(self.probs, axis = 0)
        true,false = proba[:,0], proba[:,1] # Get the predictions in seperate variables
       # d = d = {'false': false, 'true':true}
        #df = pd.DataFrame(data=d)
        sns.histplot([true, false], bins=100, stat='count')
        #df.plot.hist(bins=bins, alpha=alpha).get_figure().savefig('HerlevDistribution')

def main_w_hist(run_tile_creator = False,
                run_tile_downloader = False,
                run_tile_processor = True,
                municipality= 'Herlev',
                plot_dist = True
                ):

    # ------- Read configuration -------
    config_file = 'config.yml'
    with open(config_file, 'rb') as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)

    tile_coords_path = f"data/coords/Tile_coords_{municipality}.pickle"
    geojson_path = conf.get('geojson_path', 'utils/deutschlandGeoJSON/2_bundeslaender/1_sehr_hoch.geo.json')
    downloaded_path = conf.get('downloaded_path', 'logs/processing/DownloadedTiles.csv')
    processed_path = conf.get('processed_path','logs/processing/Processed.csv')

    # ------- GeoJsonHandler provides utility functions -------
    municipality_handler = GeoJsonHandler(geojson_path, municipality)                        # HER er ændret

    # ------- TileDownloader downloads tiles from openNRW in a multi-threaded fashion -------
    if run_tile_processor:
        tileProcessor = TileProcessorWithHist(configuration=conf, polygon=municipality_handler.polygon)
        tileProcessor.run()
        if plot_dist:
            tileProcessor.plot_distribution()

if __name__ == '__main__':
    main_w_hist(run_tile_creator=False,
                run_tile_downloader=False,
                run_tile_processor=True,
                municipality='Herlev',
                plot_dist= True)





