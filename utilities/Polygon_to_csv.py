import pickle
from itertools import chain
import numpy as np
from shapely.geometry import Polygon, MultiPolygon
from shapely.ops import cascaded_union, unary_union
from matplotlib import pyplot as plt
from shapely.geometry import Point
from src.utils.geojson_handler_utils import GeoJsonHandler
import json

with open('../municipalities.geojson') as f:
    data = json.load(f)

muni = "Gentofte"

Municipality = GeoJsonHandler(input_dir='../municipalities.geojson', input_munic=muni)
new_shape = Municipality.definePolygon()

import csv
listen = list(new_shape.exterior.coords)

file = open(f'../x{muni}_data/{muni}_poly.csv', 'w+', newline='')
with file:
    write = csv.writer(file)
    write.writerow(['lon', 'lat'])
    write.writerows(listen)
print(f"Done making csv-file with polygon of {muni}" )

"""
for geom in new_shape.geoms:
    plt.plot(*geom.exterior.xy)

    # Set (current) axis to be equal before showing plot
    #plt.gca().axis("equal")
plt.axis('square')

plt.show()
"""


