#@@ -1,218 +0,0 @@
import torch
from torchvision import datasets, models, transforms, utils
from torchvision.models import Inception3
import requests
import os
import shutil
from pathlib import Path
from PIL import Image
from torch.nn import functional as F
import numpy as np

class TileProcessor(object):

    def __init__(self):

        # Execute on gpu, if available
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        #self.device = torch.device("cpu")
        # ------ Load model configuration ------
        #self.threshold = configuration['threshold']

        # Batch size should be as large as possible to speed up the classification process
        #self.batch_size = configuration['batch_size']

        #self.input_size = configuration['input_size']

        # ------ Specify required input directories ------
        self.checkpoint_path = "Tyskland/models/inceptionv3_weights.tar"

        #self.tile_dir = configuration['tile_dir']

       # ------ Specify required output directories ------
        #self.pv_db_path = configuration['pv_db_path']

        #self.processed_path = configuration['processed_path']

        #self.not_processed_path = configuration['not_processed_path']

        # ------ Load model and dataset ------
        self.model = self.loadModel()

        #self.dataset = NrwDataset(self.tile_dir)

        # ------ Set auxiliary instance variables ------
        #self.polygon = polygon

        # Avg. earth radius in meters
        #self.radius = 6371000

        # Square side length in meters
        #self.side = 16

        # dlat spans a distance of 16 meters in north-south direction:
        #self.dlat = (self.side * 360) / (2 * np.pi * self.radius)

    def loadModel(self):
        # Specify model architecture
        model = Inception3(num_classes=2, aux_logits=True, transform_input=False)
        model = model.to(self.device)

        # Load old parameters
        checkpoint = torch.load(self.checkpoint_path, map_location=self.device)

        if self.checkpoint_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters

            model.load_state_dict(checkpoint['model_state_dict'])

        else:

            print("Nope")

        return model

class TileProcessor2(object):

    def __init__(self):

        # Execute on gpu, if available
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        # ------ Load model configuration ------
        #self.threshold = configuration['threshold']

        # Batch size should be as large as possible to speed up the classification process
        #self.batch_size = configuration['batch_size']

        #self.input_size = configuration['input_size']

        # ------ Specify required input directories ------
        self.checkpoint_path = "Tyskland/models/inceptionv3_weights.tar"

        #self.tile_dir = configuration['tile_dir']

       # ------ Specify required output directories ------
        #self.pv_db_path = configuration['pv_db_path']

        #self.processed_path = configuration['processed_path']

        #self.not_processed_path = configuration['not_processed_path']

        # ------ Load model and dataset ------
        self.model = self.loadModel()

        #self.dataset = NrwDataset(self.tile_dir)

        # ------ Set auxiliary instance variables ------
        #self.polygon = polygon

        # Avg. earth radius in meters
        #self.radius = 6371000

        # Square side length in meters
        #self.side = 16

        # dlat spans a distance of 16 meters in north-south direction:
        #self.dlat = (self.side * 360) / (2 * np.pi * self.radius)

    def loadModel(self):
        # Specify model architecture
        model = Inception3(num_classes=2, aux_logits = True, transform_input=False)
        model = model.to(self.device)

        # Load old parameters
        checkpoint = torch.load(self.checkpoint_path, map_location = self.device)

        if self.checkpoint_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters

            model.load_state_dict(checkpoint['model_state_dict'])

       # else:

            #model.load_state_dict(checkpoint)

        return model


if __name__ == '__main__':
    model1 = TileProcessor()
    model2 = TileProcessor2()


    tile = Image.open(Path("tyskland/PV system 3.png"))
    if not tile.mode == 'RGB':
        tile = tile.convert('RGB')
    print(tile.size)

    tile2 = Image.open(Path("tyskland/PV system 3.png"))
    if not tile2.mode == 'RGB':
        tile2 = tile2.convert('RGB')
    print(tile2.size)


    tile = np.array(tile)
    #tile2 = np.array(tile2)

    images = []

    for i in range(9):
        images.append(tile)

    #images.append(tile2)


    print(len(images))

    #
    # trans = transforms.Compose([
    #             transforms.Resize(299),
    #             transforms.ToTensor(),
    #             transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])])
    #
    # images_sub = [torch.unsqueeze(trans(Image.fromarray(image)),0) for image in images]
    #
    # images_sub = torch.cat(images_sub,dim=0)
    #
    # print(images_sub.size())
    #
    # outputs = model1.model(images_sub.to(model1.device))
    #
    # print(outputs)
    #
    # prob = F.softmax(outputs[0], dim=1)
    #
    # prob = prob.cpu().detach().numpy()
    #
    # print(prob)



    ##  Download picture from Danish WMS.
    current_save_path = os.path.join("testpicture" + ".png")
    #

    #url = "https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar_wmts/1.0.0/WMTS?username=IWWMVYOYPJ&password=2712.Carl&SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&STYLE=default&FORMAT=image/jpeg&TILEMATRIXSET=KortforsyningTilingDK&TILEMATRIX=3&TILEROW=6&TILECOL=8&Layer=orto_foraar_wmts" url2 = "https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar/1.0.0/WMS?username=IWWMVYOYPJ&password=2712.Carl&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=526669.5629929862916,6141966.641448142938,563371.9738442131784,6165335.754607322626&CRS=EPSG:25832&WIDTH=768&HEIGHT=489&STYLES=&FORMAT=image/jpeg&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&Layers=orto_foraar"
    url3 = "https://services.datafordeler.dk/GeoDanmarkOrto/orto_foraar/1.0.0/WMS?username=IWWMVYOYPJ&password=2712.Carl&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=526669.5629929862916,6141966.641448142938,563371.9738442131784,6165335.754607322626&CRS=EPSG:25832&WIDTH=10000&HEIGHT=10000&STYLES=&FORMAT=image/png&DPI=1000&MAP_RESOLUTION=300&FORMAT_OPTIONS=dpi:300&Layers=orto_foraar_12_5"
    #
    response = requests.get(url3, stream=True)
    #
    with open(current_save_path, 'wb') as out_file:
        response.raw.decode_content = True

        shutil.copyfileobj(response.raw, out_file)

    del response
    print(current_save_path + " has been downloaded")



    ## Parameter check between models:
    # l1 = []
    # l2 = []
    #
    # for param in model1.model.parameters():
    #     l1.append(param)
    #
    # for param in model2.model.parameters():
    #     l2.append(param)
    #
    #
    # print(l1[15] == l2[15])